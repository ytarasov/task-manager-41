package ru.t1.ytarasov.tm.dto.request.system;

import lombok.NoArgsConstructor;
import ru.t1.ytarasov.tm.dto.request.AbstractRequest;

@NoArgsConstructor
public final class ApplicationVersionRequest extends AbstractRequest {
}
