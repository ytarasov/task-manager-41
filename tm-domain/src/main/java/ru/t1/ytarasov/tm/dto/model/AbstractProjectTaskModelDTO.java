package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractProjectTaskModelDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Column
    @NotNull
    protected String name = "";

    @Column
    @NotNull
    protected String description = "";

    @Column
    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    protected Date created = new Date();

    public AbstractProjectTaskModelDTO(@NotNull String name) {
        this.name = name;
    }

    public AbstractProjectTaskModelDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public AbstractProjectTaskModelDTO(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    public AbstractProjectTaskModelDTO(@NotNull String name, @NotNull String description, @NotNull Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

}
