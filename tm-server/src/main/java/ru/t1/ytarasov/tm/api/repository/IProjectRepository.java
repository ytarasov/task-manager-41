package ru.t1.ytarasov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @NotNull
    @Select("SELECT * FROM app_project")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAll();

    @NotNull
    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAllWithUserId(@NotNull @Param("userId") String  userId);

    @Nullable
    @Select("SELECT * FROM app_project ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAllOrderByStatus();

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAllWithUserIdOrderByStatus(@NotNull @Param("userId") final String userId);

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAllWithUserOrderByName(@NotNull @Param("userId") final String userId);

    @Nullable
    @Select("SELECT * FROM app_project ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAllOrderByName();

    @Nullable
    @Select("SELECT * FROM app_project ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAllOrderByCreated();

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    List<ProjectDTO> findAllWithUserIdOrderByCreated(@NotNull @Param("userId") final String userId);

    @Select("SELECT COUNT(*) FROM app_project")
    int getSize();

    @Select("SELECT COUNT(*) FROM app_project WHERE user_id = #{userId}")
    int getSizeWithUserId(@NotNull @Param("userId") final String userId);

    @Insert("INSERT INTO app_project " +
            "(id, name, description, created, status, user_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{created}, #{status}, #{userId})")
    void add(@NotNull ProjectDTO model);

    @Nullable
    @Select("SELECT * FROM app_project WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    ProjectDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM app_project WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "userId", column = "user_id"),
    })
    ProjectDTO findOneByIdWithUserId(@NotNull @Param("userId") final String userId,
                                     @NotNull @Param("id") final String id);

    @Delete("DELETE FROM app_project")
    void clear();

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void clearByUserId(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM app_project WHERE id = #{id} AND user_id = #{userId}")
    void remove(ProjectDTO model);

    @Update("UPDATE app_project SET " +
            "name = #{name}, description = #{description}, created = #{created}," +
            " status = #{status}, user_id = #{userId} WHERE id = #{id}")
    void update(@NotNull final ProjectDTO project);

}
