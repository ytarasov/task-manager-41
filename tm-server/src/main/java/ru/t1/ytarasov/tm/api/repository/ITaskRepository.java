package ru.t1.ytarasov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    @Select("SELECT * FROM app_task")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<TaskDTO> findAll();

    @NotNull
    @Select("SELECT * FROM app_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<TaskDTO> findAllWithUserId(@NotNull @Param("userId") String  userId);

    @Nullable
    @Select("SELECT * FROM app_task ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    List<TaskDTO> findAllOrderByStatus();

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    List<TaskDTO> findAllWithUserIdOrderByStatus(@NotNull @Param("userId") final String userId);

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    List<TaskDTO> findAllWithUserIdOrderByName(@NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM app_task ORDER BY name")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    List<TaskDTO> findAllOrderByName();

    @Nullable
    @Select("SELECT * FROM app_task ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    List<TaskDTO> findAllOrderByCreated();

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    List<TaskDTO> findAllWithUserIdOrderByCreated(@NotNull final String userId);

    @Select("SELECT COUNT(*) FROM app_task")
    int getSize();

    @Select("SELECT COUNT(*) FROM app_task WHERE user_id = #{userId}")
    int getSizeWithUserId(@NotNull @Param("userId") final String userId);

    @Insert("INSERT INTO app_task " +
            "(id, name, description, created, status, project_id, user_id) " +
            "VALUES(#{id}, #{name}, #{description}, #{created}, #{status}, #{projectId}, #{userId})")
    void add(@NotNull TaskDTO model);

    @Nullable
    @Select("SELECT * FROM app_task WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    TaskDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    TaskDTO findOneByIdWithUserId(@NotNull @Param("userId") final String userId,
                                  @NotNull @Param("id") final String id
    );

    @Delete("DELETE FROM app_task")
    void clear();

    @Delete("DELETE FROM app_task WHERE user_id = #{userId}")
    void clearWithUserId(@NotNull @Param("userId") final String userId);

    @Delete("DELETE FROM app_task WHERE id = #{id} AND user_id = #{userId}")
    void remove(TaskDTO model);

    @Update("UPDATE app_task SET " +
            "name = #{name}, description = #{description}, created = #{created}," +
            " status = #{status}, project_id = #{projectId}, user_id = #{userId} WHERE id = #{id}")
    void update(@NotNull final TaskDTO task);

    @NotNull
    @Select("SELECT * FROM app_task WHERE project_id = #{projectId} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "created", column = "created"),
            @Result(property = "status", column = "status"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
    })
    List<TaskDTO> findAllTasksByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

}
