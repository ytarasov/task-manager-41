package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

public interface ISessionService {

    @NotNull
    SessionDTO add(@Nullable SessionDTO session) throws Exception;

    @Nullable
    SessionDTO findOneById(@Nullable final String id) throws Exception;

    boolean existsById(@Nullable final String id) throws Exception;

    @NotNull
    SessionDTO remove(@Nullable final SessionDTO session) throws Exception;
}
