package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ytarasov.tm.api.service.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.dto.request.task.*;
import ru.t1.ytarasov.tm.dto.response.task.*;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.taskService = serviceLocator.getTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = taskService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<TaskDTO> tasks = taskService.findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    ) {
        check(request);
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.bindTaskToProject(userId, taskId, projectId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        projectTaskService.unbindTaskFromProject(userId, taskId, projectId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskShowByProjectIdResponse showTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByProjectIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @NotNull final List<TaskDTO> tasks = taskService.findAllTasksByProjectId(userId, projectId);
        return new TaskShowByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, taskId, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, taskId, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Status status = request.getStatus();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, taskId, status);
        return new TaskChangeStatusByIdResponse(task);
    }


    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final TaskDTO task = taskService.updateById(userId, taskId, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final TaskDTO task = taskService.removeById(userId, taskId);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        taskService.clear(userId);
        return new TaskClearResponse();
    }

}
