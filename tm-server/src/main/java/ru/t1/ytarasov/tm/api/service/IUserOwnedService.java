package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IService<M> {

    void clear(@Nullable String userId) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws Exception;

    @Nullable
    M remove(@Nullable String userId, @Nullable M model) throws Exception;

}
